#include <windows.h>
#include <commctrl.h>
#include <stdio.h>

#include "resource.h"

#define nano_template "Nanomite_%03X:\r\n\
                 DWORD offset   %08X\r\n\
                 BYTE  unk_1    %02X\r\n\
                 DWORD jcc_len  %08X\r\n\
                 WORD  jcc_kind %04X\r\n\
                 WORD  unk_2    %04X\r\n\r\n"

#define patch_header "var ImageBase\r\n\
mov ImageBase, %s\r\n"

#define patch_template(sign)  "asm ImageBase+%08X, \"%s ImageBase+%08X" sign "%08X\"\r\n"

#define Msg_UnkNano MessageBoxA(hwndDlg, "Unknown nanomite found!\nPlease write a PM to SmilingWolf over at Tuts4You.", "Error", MB_ICONERROR)

typedef struct
{
    DWORD offset;
    BYTE  unk_1;
    DWORD jcc_len;
    WORD  jcc_kind;
    WORD  unk_2;
} NanoStruct;

HINSTANCE hInst;

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        DragAcceptFiles(hwndDlg, TRUE);
        SetDlgItemTextA(hwndDlg, IDC_FILENAME, "Drag&Drop a file here");
        SetDlgItemTextA(hwndDlg, IDC_IMAGEBASE, "0000000000400000");
        SendDlgItemMessageA(hwndDlg, IDC_IMAGEBASE, EM_SETLIMITTEXT, 16, 0);
    }
    return TRUE;

    case WM_DROPFILES:
    {
        HDROP hDrop = (HDROP)wParam;
        char filename[MAX_PATH] = { 0x00 };
        DragQueryFileA(hDrop, 0, filename, MAX_PATH);
        SetDlgItemTextA(hwndDlg, IDC_FILENAME, filename);
        DragFinish(hDrop);
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_GO:
        {
            HANDLE infile;
            HANDLE outfile;
            HANDLE patchfile;

            NanoStruct ns;

            unsigned int i;
            unsigned int file_size;
            unsigned long dwBytesWritten;

            unsigned char offset[4];
            unsigned char unk_1[1];
            unsigned char jcc_len[4];
            unsigned char jcc_kind[2];
            unsigned char unk_2[2];

            char tablefile[FILENAME_MAX] = { 0x00 };
            char fixfile[FILENAME_MAX] = { 0x00 };
            char logfile[FILENAME_MAX] = { 0x00 };

            char imagebase[17] = { 0x00 };
            char log[1024] = { 0x00 };
            char patchline[1024] = { 0x00 };

            GetDlgItemTextA(hwndDlg, IDC_FILENAME, tablefile, FILENAME_MAX);
            GetDlgItemTextA(hwndDlg, IDC_IMAGEBASE, imagebase, 17);

            infile = CreateFile(tablefile, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

            file_size = GetFileSize(infile, NULL);
            if(!file_size || file_size % 13 != 0)
            {
                MessageBoxA(hwndDlg, "The nanomites table MUST have a size which is a multiple of 13.\nCheck if you have correctly dumped it.", "Error", MB_ICONERROR);
                break;
            }

            i = strlen(tablefile);
            while(tablefile[i] != '\\')
            {
                i--;
            }
            memcpy(fixfile, tablefile, i);
            memcpy(logfile, tablefile, i);

            strcat(fixfile, "\\NanoFix.txt");
            patchfile = CreateFileA(fixfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
            strcat(logfile, "\\NanoTable.log");
            outfile = CreateFileA(logfile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

            sprintf(patchline, patch_header, imagebase);
            WriteFile(patchfile, patchline, strlen(patchline), &dwBytesWritten, NULL);

            for(i = 0; i < file_size; i += 13)
            {
                ReadFile(infile, offset, sizeof(offset), &dwBytesWritten, NULL);
                ReadFile(infile, unk_1, sizeof(unk_1), &dwBytesWritten, NULL);
                ReadFile(infile, jcc_len, sizeof(jcc_len), &dwBytesWritten, NULL);
                ReadFile(infile, jcc_kind, sizeof(jcc_kind), &dwBytesWritten, NULL);
                ReadFile(infile, unk_2, sizeof(unk_2), &dwBytesWritten, NULL);

                ns.offset   = offset[3] * 0x1000000 + offset[2] * 0x10000 + offset[1] * 0x100 + offset[0];
                ns.unk_1    = unk_1[0];
                ns.jcc_len  = jcc_len[3] * 0x1000000 + jcc_len[2] * 0x10000 + jcc_len[1] * 0x100 + jcc_len[0];
                ns.jcc_kind = jcc_kind[1] * 0x100 + jcc_kind[0];
                ns.unk_2    = unk_2[1] * 0x100 + unk_2[0];

                // Write the log with the nanomites infos
                // FIXME, I beg you, this thing is horrible
                sprintf(log, nano_template, i / 13, ns.offset, ns.unk_1, ns.jcc_len, ns.jcc_kind, ns.unk_2);
                WriteFile(outfile, log, strlen(log), &dwBytesWritten, NULL);
                SetFilePointer(outfile, 0, NULL, FILE_END);

                // Write the script to patch the exe
                switch(unk_1[0])
                {
                case 00:
                {
                    if(jcc_kind[1] == 00)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 00 + jcc_kind = 0000 -> JE  Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "je  ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   //unk_1 = 00 + jcc_kind = 0001 -> JNZ Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "jnz ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else if(jcc_kind[1] == 01)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 00 + jcc_kind = 0100 -> JE  Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "je  ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   // unk_1 = 00 + jcc_kind = 0101 -> JNZ Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "jnz ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else
                    {
                        Msg_UnkNano;
                    }
                    break;
                }
                case 01:
                {
                    if(jcc_kind[1] == 00)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 01 + jcc_kind = 0000 -> JB Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "jb  ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   // unk_1 = 01 + jcc_kind = 0001 -> JNB Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "jnb ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else if(jcc_kind[1] == 01)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 01 + jcc_kind = 0100 -> JB  Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "jb  ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   // unk_1 = 01 + jcc_kind = 0101 -> JNB  Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "jnb ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else
                    {
                        Msg_UnkNano;
                    }
                    break;
                }
                case 02:
                {
                    if(jcc_kind[1] == 00)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 02 + jcc_kind = 0000 -> JBE Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "jbe ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   // unk_1 = 02 + jcc_kind = 0001 -> JA  Forward
                        {
                            sprintf(patchline, patch_template("+"), ns.offset, "ja  ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else if(jcc_kind[1] == 01)
                    {
                        if(jcc_kind[0] == 00)   // unk_1 = 02 + jcc_kind = 0100 -> JBE Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "jbe ", ns.offset, ns.jcc_len);
                        }
                        else if(jcc_kind[0] == 01)   // unk_1 = 02 + jcc_kind = 0101 -> JA  Backward
                        {
                            sprintf(patchline, patch_template("-"), ns.offset, "ja  ", ns.offset, ns.jcc_len);
                        }
                        else
                        {
                            Msg_UnkNano;
                        }
                    }
                    else
                    {
                        Msg_UnkNano;
                    }
                    break;
                }
                default:
                {
                    Msg_UnkNano;
                    break;
                }
                }
                WriteFile(patchfile, patchline, strlen(patchline), &dwBytesWritten, NULL);
                SetFilePointer(outfile, 0, NULL, FILE_END);
            }

            WriteFile(patchfile, "ret", 3, &dwBytesWritten, NULL);

            CloseHandle(infile);
            CloseHandle(outfile);
            CloseHandle(patchfile);
        }
        }
    }
    return TRUE;
    }
    return FALSE;
}


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst = hInstance;
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
